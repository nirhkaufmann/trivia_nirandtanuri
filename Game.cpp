#include "Game.h"

Game::Game(const vector<User*>& players, int questionNo, Database& db)
{
	_users = players;
	_question_no = questionNo;
	_db = Database(db);
	if(_db.insertNewGame()==-1) throw exception("game could not be added to the Database");
	_questions=_db.initQuestions(questionNo);
	for (int i = 0; i < _users.size(); ++i)
	{
		_results.insert(pair<string, int>(_users[i]->getUsername(), 0));
		_users[i]->setGame(this);
	}


	
}
Game::~Game()
{
	
	while (!_users.empty() || !_questions.empty())
	{
		if (!_users.empty())
		{
			delete _users.front();
			_users.erase(_users.begin());
		}
		if (!_questions.empty())
		{
			delete _questions.front();
			_questions.erase(_questions.begin());
		}
	}

}
void Game::sendFirstQuestion()
{
	sendQuestionToAllUsers();
}
void Game::handleFinishGame()
{
	_db.updateGameStatus(1);
	string m = END_GAME+' '+to_string(_users.size())+"##";
	for (int i = 0; i < _users.size(); i++)
	{
		m += _results[_users[i]->getUsername()] + "##";
	}
	sendMessageToAllUsers(m);
	for (int i = 0; i < _users.size(); i++)
	{
		delete _users[i];
		_users[i] = nullptr;
	}

}
bool Game::handleNextTurn()
{
	if (_users.size() == 0)
	{
		handleFinishGame();
		return false;
	}
	if (_currentTurnAnswers == _users.size())
	{
		if (_currQuestionIndex == _questions.size())
		{
			handleFinishGame();
			return true;
		}
		sendQuestionToAllUsers();
		_currQuestionIndex++;
		return true;
	}
	return false;


}
bool Game::handleAnwerFromUser(User* user, int answerNo, int time)
{
	_currentTurnAnswers++;
	bool correct = answerNo == _questions[_currQuestionIndex]->getCorrectAnswerIndex();
	if (correct) _results[user->getUsername()]++;
	_db.addAnswerToPlayer(_questions[_currQuestionIndex]->getId(), user->getUsername(), answerNo, correct ? _questions[_currQuestionIndex]->getAnswers()[answerNo] : "", correct, time);
	try
	{
		user->send(RESPONE_ANSWER_CORRECTNESS + to_string(correct ? 1 : 0));
	}
	catch (exception exc)
	{
		cout << exc.what() << endl;
	}
	return handleNextTurn();
}
bool Game::leaveGame(User* user)
{
	int i = 0;
	while(i < _users.size())
	{
		if (_users[i] == user)
		{
			delete _users[i];
			_users.erase(_users.begin() + i);
			return true;
		}
	}
	return handleNextTurn();

}
int Game::getId()
{
	
}
bool Game::insertGameToDB()
{
	_db.insertNewGame();
	
}
void Game::initQuestionFromDB(){}
void Game::sendQuestionToAllUsers()
{
	string q = SEND_QUESTION;
	q += "###" + _questions[_currQuestionIndex]->getQuestion();
	for (int i = 0; i < 4; i++) q += "###" + _questions[_currQuestionIndex]->getAnswers()[i];
	_currentTurnAnswers = 0;
	sendMessageToAllUsers(q);


}

void Game::sendMessageToAllUsers(string message)
{
	for (int i = 0; i < _users.size(); i++)
	{
		try
		{
			_users[i]->send(message);
		}
		catch (exception exc)
		{
			cout << exc.what() << endl;
		}
	}
}