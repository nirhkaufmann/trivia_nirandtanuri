#pragma once
#include <string>
#include "Room.h"
#include "Game.h"
#include <WinSock2.h>
#include "Helper.h"
#include "Protocol.h"
using namespace std;

class User
{
private:
	string _username;
	Room* _currRoom;
	Game* _currGame;
	SOCKET _sock;
public:
	User(string, SOCKET);
	void send(string);
	string getUsername();
	SOCKET getSocket();
	Room* getRoom();
	Game* getGame();
	void setGame(Game*);
	void clearGame();
	void clearRoom();
	bool createRoom(int, string, int, int, int);
	bool joinRoom(Room*);
	void leaveRoom();
	int closeRoom();
	bool leaveGame();
	bool operator==(User*);
	bool operator!=(User*);



};