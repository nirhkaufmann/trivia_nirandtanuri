#pragma once
#include "User.h"
#include <map>
#include <string>
#include "DataBase.h"
#include <vector>
#include "Question.h"
using namespace std;
class Game
{
private:
	vector<Question*> _questions;
	vector<User*> _users;
	int _question_no;
	int _currQuestionIndex;
	Database& _db;
	map<string, int> _results;
	int _currentTurnAnswers;
	
	bool insertGameToDB();
	void initQuestionFromDB();
	void sendQuestionToAllUsers();
	void sendMessageToAllUsers(string);
public:
	Game(const vector<User*>&, int, Database&);
	~Game();
	void sendFirstQuestion();
	void handleFinishGame();
	bool handleNextTurn();
	bool handleAnwerFromUser(User*, int, int);
	bool leaveGame(User*);
	int getId();

};