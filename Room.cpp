#include "Room.h"

Room::Room(int id, User* admin, string name, int maxUsers, int questionNo, int questionTime)
{
	_id = id;
	_admin = admin;
	_name = name;
	_maxUsers = maxUsers;
	_questionNo = questionNo;
	_questionTime = questionTime;
	_users.push_back(_admin);
}
string Room::getUsersAsString(vector<User*> usersList, User* excludeUser)
{
	string s = "";
	for (int i = 0; i < usersList.size(); i++)
	{
		if (!excludeUser || _users[i] != excludeUser) s += usersList[i]->getUsername() + "##";
	}
	return s;
}


string Room::getUsersListMessage()
{
	string m = SEND_ROOM_USERS;
	m += to_string(this->_users.size())+"##";
	m += getUsersAsString(this->_users, nullptr);
	return m;

}

void Room::sendMessage(User* excludeUser, string message)
{
	try
	{
		for (int i = 0; i < _users.size(); ++i)
		{
			if (!excludeUser||_users[i]!=excludeUser) _users[i]->send(message);
		}

	}
	catch (exception exc)
	{
		cout << exc.what() << endl;
	}
}

void Room::sendMessage(string message)
{
	sendMessage(nullptr, message);
}

bool Room::joinRoom(User* user)
{
	if (_users.size() >= _maxUsers) return false;
	_users.push_back(user);
	sendMessage(getUsersListMessage());
}

void Room::leaveRoom(User* user)
{
	int i = 0;
	while (i < _users.size() && _users[i] != user) i++;
	if (i < _users.size()) _users.erase(_users.begin() + i);
}

int Room::closeRoom(User* user)
{
	if (user != _admin) return -1;
	sendMessage(CLOSE_ROOM_RESPONSE);
	for (int i = 0; i < _users.size(); i++) 
		if (_users[i]!=_admin)
			_users[i]->clearRoom();
	return _id;

}
