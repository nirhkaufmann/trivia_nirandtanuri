#pragma once
#include <string>
using namespace std;

class Question
{
private:
	 string question;
	 string answers[4];
	 int correctAnswerIndex;
	 int id;

public:
	Question(int, string, string, string, string, string);
	string getQuestion();
	string* getAnswers();
	int getCorrectAnswerIndex();
	int getId();






};