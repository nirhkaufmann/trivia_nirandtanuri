#include "Validator.h"

bool Validator::isPasswordValid(string password)
{
	if (password.length() < 4)
	{
		return false;
	}

	bool numCount = false;
	bool lowCaseCount = false;
	bool upCaseCount = false;

	for (int i = 0; i < password.length(); i++)
	{
		if (password.at(i) == ' ')
		{
			return false;
		}

		if (password.at(i) >= '0' && password.at(i) <= '9')
		{
			numCount = true;;
		}

		if (password.at(i) >= 'A' && password.at(i) <= 'Z')
		{
			upCaseCount = true;
		}

		if (password.at(i) >= 'a' && password.at(i) <= 'z')
		{
			lowCaseCount = true;
		}

	}

	if (numCount && upCaseCount && lowCaseCount)
	{
		return true;
	}

	return false;


}


bool Validator::isUsernameValid(string username)
{
	if (username.empty())
	{
		return false;
	}

	if (!((username.at(0) >= 'A' && username.at(0) <= 'Z') || (username.at(0) >= 'a' && username.at(0) <= 'a')))
	{
		return false;
	}

	for (int i = 0; i < username.length(); i++)
	{
		if (username.at(i) == ' ')
		{
			return false;
		}
	}

	return true;
}

