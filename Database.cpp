
#include "Database.h"
#include <iostream>
std::unordered_map<string, vector<string>> results;



int Database::callback(void* notUsed, int argc, char** argv, char** azCol)
{
	
	int i;

	for (i = 0; i < argc; i++)
	{
		auto it = results.find(azCol[i]);
		if (it != results.end())
		{
			it->second.push_back(argv[i]);
		}
		else
		{
			pair<string, vector<string>> p;
			p.first = azCol[i];
			p.second.push_back(argv[i]);
			results.insert(p);
		}
	}

	return 0;
}

void Database:: clearTable()
{
	for (auto it = results.begin(); it != results.end(); ++it)
	{
		it->second.clear();
	}
	results.clear();
}

void Database::printTable()
{
	auto iter = results.end();
	iter--;
	//	int size = results.begin()->second.size();
	int size = iter->second.size();
	for (int i = -2; i < size; i++)
	{
		for (auto it = results.begin(); it != results.end(); ++it)
		{
			if (i == -2)
			{
				//cout << it->first << " ";
				printf("|%*s|", 13, it->first.c_str());
			}
			else if (i == -1)
				cout << "_______________";
			else
				//	cout << it->second.at(i) << " ";
				printf("|%*s|", 13, it->second.at(i).c_str());
		}
		cout << endl;
	}
}
Database::Database()
{
	
	int rc;
	char* sql;
	char *zErrMsg = 0;
	bool flag = true;

	
	rc = sqlite3_open("trivia.db", &db);

	if (rc)
	{
		sqlite3_close(db);
		system("Pause");
	}

	//first table
	sql = "create table t_users(username string primary key not null,password string not null,email string not null);";
		

	rc = sqlite3_exec(db, sql,callback,0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		fprintf(stderr, "SQL error: %s\n", zErrMsg);
		sqlite3_free(zErrMsg);
	}

	//second table

	sql = "create table t_games(game_id integer primary key autoincrement not null,status integer not null, start_time datetime not null,end_time datetime);";


	rc = sqlite3_exec(db, sql,callback, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		fprintf(stderr, "SQL error: %s\n", zErrMsg);
		sqlite3_free(zErrMsg);
	}

	//third table

	sql = "create table t_questions(question_id integer primary key autoincrement not null,question string not null,correct_ans string not null,ans2 string not null,ans3 string not null,ans4 string not null);";


	rc = sqlite3_exec(db, sql, callback, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		fprintf(stderr, "SQL error: %s\n", zErrMsg);
		sqlite3_free(zErrMsg);
	}

	//fourth table

	sql = "create table t_players_answers(game_id integer not null,username string not null,question_id integer not null,player_answer string not null,is_correct integer not null,answer_time integer not null,primary key(game_id,username,question_id),foreign key(game_id) REFERENCES t_games(game_id),foreign key(username) REFERENCES t_users(username),foreign key(question_id) REFERENCES t_questions(question_id));";


	rc = sqlite3_exec(db, sql, callback, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		fprintf(stderr, "SQL error: %s\n", zErrMsg);
		sqlite3_free(zErrMsg);
	}

	clearTable();

	sql = "insert into t_questions(question,correct_ans,ans2,ans3,ans4) values (\"Who was the first prime minister?\",\"david ben gurion\",\"menahem begin\",\"bibi netanyahu\",\"shimon peres\")";


	rc = sqlite3_exec(db, sql, callback, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		fprintf(stderr, "SQL error: %s\n", zErrMsg);
		sqlite3_free(zErrMsg);
	}

	clearTable();

	sql = "insert into t_questions(question,correct_ans,ans2,ans3,ans4) values (\"Who was Pablo Picasso?\",\"artist\",\"athlete\",\"drug dealer\",\"leader\")";


	rc = sqlite3_exec(db, sql, callback, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		fprintf(stderr, "SQL error: %s\n", zErrMsg);
		sqlite3_free(zErrMsg);
	}

	clearTable();

	sql = "insert into t_questions(question,correct_ans,ans2,ans3,ans4) values (\"What is the largest country in the world?\",\"Russia\",\"Canada\",\"China\",\"USA\")";


	rc = sqlite3_exec(db, sql, callback, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		fprintf(stderr, "SQL error: %s\n", zErrMsg);
		sqlite3_free(zErrMsg);
	}

	clearTable();

	sql = "insert into t_questions(question,correct_ans,ans2,ans3,ans4) values (\"Which Israeli celebrity has the most followers on facebook?\",\"Gal Gadot\",\"Bar Refaeli\",\"Bibi Netanyahu\",\"Omri Casspi\")";


	rc = sqlite3_exec(db, sql, callback, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		fprintf(stderr, "SQL error: %s\n", zErrMsg);
		sqlite3_free(zErrMsg);
	}

	clearTable();

	sql = "insert into t_questions(question,correct_ans,ans2,ans3,ans4) values (\"Where is IKEA originally from?\",\"Sweden\",\"USA\",\"Finland\",\"Germany\")";


	rc = sqlite3_exec(db, sql, callback, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		fprintf(stderr, "SQL error: %s\n", zErrMsg);
		sqlite3_free(zErrMsg);
	}

	clearTable();

	


}

Database::~Database()
{
	sqlite3_close(db);
}

bool Database::isUserExists(string input)
{
	int rc;
	char *zErrMsg = 0;
	bool flag = true;
	string sql = "Select username from t_users where username = ";
	sql = sql+ '"' + input+'"';
	const char *cstr = sql.c_str();
	clearTable();

	rc = sqlite3_exec(db, cstr, callback,0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		fprintf(stderr, "SQL error: %s\n", zErrMsg);
		sqlite3_free(zErrMsg);
		
	}

	else
	{
		
		std::unordered_map<string, vector<string>>::const_iterator got = results.find("username");
		if (got == results.end())
		{
			clearTable();
			return false;
		}

		clearTable();
		return true;
	}
	
	
}

bool Database::addNewUser(string Iusername, string Ipassword, string Iemail)
{
	int rc;
	char *zErrMsg = 0;
	bool flag = true;
	string sql = "insert into t_users(username, password, email) values(";
	sql += '"' + Iusername + '"' + "," + '"' + Ipassword + '"' + "," + '"' + Iemail + '"'+ ")";
	const char *cstr = sql.c_str();

	
	rc = sqlite3_exec(db, cstr, callback, 0, &zErrMsg);

	if (rc != SQLITE_OK)
	{
		fprintf(stderr, "SQL error: %s\n", zErrMsg);
		sqlite3_free(zErrMsg);
		return false;
	}

	return true;
}

bool Database::isUserAndPassMatch(string Iusername, string Ipassword)
{
	
	int rc;
	char *zErrMsg = 0;
	bool flag = true;
	string sql = "Select username from t_users where username =  ";
	sql += '"'+Iusername +'"'+ " and password = " + '"'+Ipassword +'"'+ ";";
	const char *cstr = sql.c_str();
	

	rc = sqlite3_exec(db, cstr, callback, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		fprintf(stderr, "SQL error: %s\n", zErrMsg);
		sqlite3_free(zErrMsg);

	}

	else
	{

		std::unordered_map<string, vector<string>>::const_iterator got = results.find("username");
		if (got == results.end())
		{
			clearTable();
			return false;
		}

		clearTable();
		return true;
	}

}


int Database::insertNewGame()
{
	int rc;
	char *zErrMsg = 0;
	bool flag = true;
	string sql = "insert into t_games(status,start_time) values(0,\"NOW\");";
	const char *cstr = sql.c_str();


	rc = sqlite3_exec(db, cstr, callback, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		fprintf(stderr, "SQL error: %s\n", zErrMsg);
		sqlite3_free(zErrMsg);
		clearTable();
		return 0;

	}

	else
	{

		clearTable();
		return sqlite3_last_insert_rowid(db);
	}

	
}

vector<Question*> Database::initQuestions(int questionsNo)
{
	vector<Question*> vec;
	int rc;
	char *zErrMsg = 0;
	bool flag = true;
	string sql = "select * from t_questions;";
	const char *cstr = sql.c_str();


	rc = sqlite3_exec(db, cstr, callback, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		fprintf(stderr, "SQL error: %s\n", zErrMsg);
		sqlite3_free(zErrMsg);
		clearTable();
		return vec;

	}

	else
	{
		
		vector<string> idVec = results.at("question_id");
		vector<string> questionVec = results.at("question");
		vector<string> correctVec = results.at("correct_ans");
		vector<string> ans2Vec = results.at("ans2");
		vector<string> ans3Vec = results.at("ans3");
		vector<string> ans4Vec = results.at("ans4");

		for (int i = 0; i < questionsNo; i++)
		{
			Question q = Question(atoi(idVec.at(i).c_str()), questionVec.at(i), correctVec.at(i), ans2Vec.at(i), ans3Vec.at(i), ans4Vec.at(i));
			vec.push_back(&q);
		}
		clearTable();
		return vec;
	}
}


bool Database::updateGameStatus(int gameID)
{
	int rc;
	char *zErrMsg = 0;
	bool flag = true;
	string sql = "Update t_games set status = 1,end_time = \"NOW\" where game_id = ";
	sql+=to_string(gameID) + ';';
	const char *cstr = sql.c_str();


	rc = sqlite3_exec(db, cstr, callback, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		fprintf(stderr, "SQL error: %s\n", zErrMsg);
		sqlite3_free(zErrMsg);
		clearTable();
		return false;

	}

	clearTable();
	return true;
}


bool Database::addAnswerToPlayer(int gameId, string username, int questionId, string answer , bool isCorrect, int answerTime)
{
	int rc;
	char *zErrMsg = 0;
	bool flag = true;
	string sql = "insert into t_players_answers(game_id,username,question_id,player_answer,is_correct,answer_time) values(";
	sql += to_string(gameId) + ',' + "\"" + username + "\""+',' + to_string(questionId) + ',' + "\"" + answer + "\""+ ',' + to_string(isCorrect) + ',' + to_string(answerTime) + ");";
	const char *cstr = sql.c_str();


	rc = sqlite3_exec(db, cstr, callback, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		fprintf(stderr, "SQL error: %s\n", zErrMsg);
		sqlite3_free(zErrMsg);
		clearTable();
		return false;

	}

	else
	{

		clearTable();
		return true;
	}
}











