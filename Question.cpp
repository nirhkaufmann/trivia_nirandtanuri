#include "Question.h"
#include <vector>
#include <algorithm>
#define ANSWERS_NUM 4

Question::Question(int id, string question, string correctAnswer, string answer2, string answer3, string answer4)
{
	this->id = id;
	this->question = question;
	this->correctAnswerIndex = rand() % 4;
	vector<int> indexArr = { 0, 1, 2, 3 };
	random_shuffle(indexArr.begin(), indexArr.end());
	this->answers[indexArr[0]] = correctAnswer;
	this->correctAnswerIndex = indexArr[0];
	this->answers[indexArr[1]] = answer2;
	this->answers[indexArr[2]] = answer3;
	this->answers[indexArr[3]] = answer4;


}
string Question::getQuestion()
{
	return this->question;
}
string* Question::getAnswers()
{
	return this->answers;
}

int Question::getCorrectAnswerIndex()
{
	return this->correctAnswerIndex;
}
int Question::getId()
{
	return this->id;
}