#pragma once
#include "Question.h"
#include "sqlite3.h"
#include <vector>
#include <string>
#include <unordered_map>
#include <iterator>
using namespace std;



extern unordered_map<string, vector<string>> results;

class Database
{

private:
	
	sqlite3* db;
	void clearTable();
	void printTable();
	int static callback(void* notUsed, int argc, char** argv, char** azCol);
	

public:


	Database();
	~Database();
	bool isUserExists(string);
	bool addNewUser(string, string, string);
	bool isUserAndPassMatch(string, string);
	vector<Question*> initQuestions(int);
	vector<string> getBestScores();
	vector<string> getPersonalStatus(string);
	int insertNewGame();
	bool updateGameStatus(int);
	bool addAnswerToPlayer(int, string, int, string, bool, int);
	


};


	
	


