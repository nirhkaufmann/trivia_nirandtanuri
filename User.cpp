#include "User.h"

User::User(string username, SOCKET sock)
{
	_username = username;
	_sock = sock;
	_currGame = nullptr;
	_currRoom = nullptr;
}
void User::send(string msg)
{
	Helper::sendData(_sock, msg);
}
void User::setGame(Game* gm)
{
	_currGame = gm;
	_currRoom = nullptr;
}
void User::clearGame()
{
	_currGame = nullptr;
}
bool User::createRoom(int roomId, string roomName, int maxUsers, int questionsNo, int questionTime)
{
	if (!_currRoom)
	{
		send(CREATE_ROOM_RESPONSE_FAIL);
		return false;
	}
	else
	{
		_currRoom = new Room(roomId, this, roomName, maxUsers, questionsNo, questionTime);
		send(CREATE_ROOM_RESPONSE_SUCCESS);
		return true;
	}
}
bool User::joinRoom(Room* newRoom)
{
	return (!_currRoom&&newRoom->joinRoom(this));
}
void User::leaveRoom()
{
	if (_currRoom) _currRoom->leaveRoom(this);
	_currRoom = nullptr;
}
int User::closeRoom()
{
	int ans = 0;
	if (!_currRoom) return -1;
	ans = _currRoom->closeRoom(this);
	if (ans == -1) return -1;
	delete (_currRoom);
	_currRoom = nullptr;
	return ans;
}
bool User::leaveGame()
{
	if (!_currGame) return false;
	bool ans = _currGame->leaveGame(this);
	_currGame = nullptr;
	return ans;
}
void User::clearRoom()
{
	_currRoom = nullptr;
}
string User::getUsername() { return _username; }
SOCKET User::getSocket() { return _sock; }
Room* User::getRoom() { return _currRoom; }
Game* User::getGame() { return _currGame; }
bool User::operator==(User* other)
{
	return this->_username == other->_username;
}
bool User::operator!=(User* other)
{
	return this->_username != other->_username;

}