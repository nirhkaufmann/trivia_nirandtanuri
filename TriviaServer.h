#include <winsock.h>
#include <map>
#include <thread>
#include "Room.h"
#include <queue>
#include "RecievedMessage.h"
#include "Database.h"
#include "Helper.h"
#include "Protocol.h"
#include "Question.h"
#include "Game.h"
#include "User.h"
#include "Validator.h"
#include <mutex>
class TriviaServer
{
private:
	SOCKET _socket;
	map<SOCKET, User*> _connectedUsers;
	Database _db;
	map<int, Room*> _roomsList;
	mutex _mtxRecievedMessages;
	queue<RecievedMessage*> _queRcvMessages;
	static int _roomIdSequence;
	void bindAndListen();

	void accept();
	void clientHandler(SOCKET);
	void safeDeleteUser(RecievedMessage*);

	User* handleSignin(RecievedMessage*);
	bool handleSignup(RecievedMessage*);
	void handleSignout(RecievedMessage*);

	void handleLeaveGame(RecievedMessage*);
	void handleStartGame(RecievedMessage*);
	void handlePlayerAnswer(RecievedMessage*);

	bool handleCreateRoom(RecievedMessage*);
	bool handleCloseRoom(RecievedMessage*);
	bool handleJoinRoom(RecievedMessage*);
	bool handleLeaveRoom(RecievedMessage*);
	void handleGetUsersInRoom(RecievedMessage*);
	void hanldeGetRooms(RecievedMessage*);

};