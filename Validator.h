#pragma once
#include <string>
using namespace std;

static class Validator
{
public:
	static bool isPasswordValid(string);
	static bool isUsernameValid(string);

};