#pragma once
#include <vector>
#include "User.h"
#include <string>
#include "Protocol.h"
#include <iostream>
using namespace std;
class Room
{
private:
	vector<User*> _users;
	User* _admin;
	int _maxUsers;
	int _questionTime;
	int _questionNo;
	string _name;
	int _id;
	string getUsersAsString(vector<User*>, User*);
	void sendMessage(string);
	void sendMessage(User*, string);
public:
	Room(int, User*, string, int, int, int);
	bool joinRoom(User*);
	void leaveRoom(User*);
	vector<User*> getUsers();
	string getUsersListMessage();
	int getQuestionNo();
	int getId();
	string getName();

	int closeRoom(User*);
	
};